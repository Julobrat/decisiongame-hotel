﻿namespace DecisionGame.FrontEnd {
    public partial class WorkOfficeForm : BaseForm {

        public WorkOfficeForm() { }
        public WorkOfficeForm(GameForm parent1) : base(parent1) {
            InitializeComponent();
            this.Size = new System.Drawing.Size(700, this.Height);
        }
    }
}