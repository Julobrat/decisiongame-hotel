﻿using System.Collections.Generic;

namespace DecisionGame.BackEnd {
    public class GameSettings {
        /// <summary>
        /// Class representing setting in the game. 
        /// </summary>
        private string companyName;

        public Dictionary<string, string> json;

        private int minimalSalary;
        public Game parent;
        public Place Place;

        public GameSettings(Game parent1, InitialSettings initialSettings) {
            parent = parent1;
            MinimalSalary = 1200;
            CompanyName = "Belweder";
            Place = initialSettings.Place;
            json = initialSettings.jsonSettings;
            getDataFromInitialSettings(initialSettings);
        }

        public GameForm gameForm => parent.parentForm;

        public int MinimalSalary {
            get => minimalSalary;
            set {
                minimalSalary = value;
                gameForm.ChangedMinimalSalaryEvent(value);
            }
        }

        public string CompanyName {
            get => companyName;
            set {
                companyName = value;
                gameForm.ChangedCompanyNameEvent(value);
            }
        }

        public void getDataFromInitialSettings(InitialSettings initSettings) { }
    }
}