﻿using System.Windows.Forms;
using DecisionGame.BackEnd;

namespace DecisionGame.FrontEnd {
    public partial class AdAgencyForm : BaseForm {
        public AdAgencyForm(GameForm parent1) : base(parent1) {
            InitializeComponent();
            this.Size = new System.Drawing.Size(700, this.Height);
        }
    }
}