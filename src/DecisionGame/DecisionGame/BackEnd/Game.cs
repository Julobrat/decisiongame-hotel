﻿using System;
using System.Collections.Generic;
using System.Linq;
using DecisionGame.Utils;

namespace DecisionGame.BackEnd {
    /// <summary>
    /// One of the main classes of the project, representing instance of game. 
    /// </summary>
    /// <description>
    /// This class holds references to all other important classes like Bank, Hotel etc.
    /// It also keeps track of rounds, settings, events and decisions.
    /// </description>

    public class Game {
        public static int numberRoomsDeluxBecomeAvailable;
        
        public Round[] AllRounds;

        public Round CurrentRound;
        public GameSettings GameSettings;

        public bool HasEnded;

        public double CompetitionCoefficient;

        public Bank Bank;
        public Hotel Hotel;
        public WorkOffice workOffice;
        public AdAgency AdAgency;

        public GameForm parentForm;

        private readonly List<RandomEvent> randomEvents;

        private int allowedRoomsToBuyCount;
        private int QuarterLength;

        private double Result = 0.0;

        public Game(GameForm parentForm1, InitialSettings settings) {
            parentForm = parentForm1;
            Hotel = new Hotel(this);
            Bank = new Bank(this);
            workOffice = new WorkOffice(this);
            AdAgency = new AdAgency(this);
            GameSettings = new GameSettings(this, settings);
            LoadRounds();
            randomEvents = RandomEvent.GenerateRandomEvents(Hotel);
        }
        /// <summary>
        /// The <c>Label</c> property represents a label
        /// for this instance.
        /// </summary>
        public int AllowedRoomsToBuyCount {
            get => allowedRoomsToBuyCount;
            set {
                allowedRoomsToBuyCount = value;
                parentForm.ChangeAllowedRoomsToBuyCount();
            }
        }

        public double calculateCompetition()
        {
            int costPlayer = RoomDelux.StaticCostPerDay();
            int competitorCost = RoomDelux.CompetitorCost;

            int costPlayer2 = RoomStandard.StaticCostPerDay();
            int competitorCost2 = RoomStandard.CompetitorCost;
            return (Math.Min((costPlayer2 + competitorCost2) / 2 * RoomDelux.StaticCostPerDay(), 1) +
                   Math.Min((costPlayer + competitorCost) / 2 * RoomDelux.StaticCostPerDay(), 1) ) / 2;
        }

        public void AdvanceToNextRound() {
            if (CurrentRound.RoundNumber >= AllRounds.Length) {
                End("Koniec Gry!");
                return;
            }

            Hotel.OldMoney = Hotel.Money;

            SimulateQuarter();
            Bank.PrepareForNextRound();
            Hotel.PrepareForNextRound();
            AdAgency.PrepareForNextRound();

            AllowedRoomsToBuyCount = int.Parse(GameSettings.json["allowedRoomsToBuyCount"]);

            CurrentRound = AllRounds[CurrentRound.RoundNumber];

            TriggerEvents();
            TriggerDecisions();
            RollEvents();

            parentForm.ChangedIncomeEvent(Hotel.Money - Hotel.OldMoney);
        }
        public void Start() {
            workOffice.hireEmploye(Profession.Cook, 1, Workers.minimumWage);
            workOffice.hireEmploye(Profession.Janitor, 1, Workers.minimumWage);
            workOffice.hireEmploye(Profession.Receptionist, 1, Workers.minimumWage);
            ApplySettings();
            // Info powitalne
            Event.ShowMessage("Witaj w naszej grze decyzyjnej. Będziesz odpowiedzialny za zarządzanie hotelem w możliwie najoptymalniejszy sposób.");
            TriggerEvents();
            TriggerDecisions();
        }

        public bool AreDecisionsToMake() {
            return Bank.AreDecisionToMake() && AdAgency.AreDecisionToMake(); 
        }

        private void SimulateQuarter() {
            CompetitionCoefficient = this.calculateCompetition();

            for (var i = 0; i < QuarterLength; ++i) Hotel.AdvanceDay();

            Hotel.Money -= Hotel.MonthlyPayments;
        }

        private void TriggerEvents() {
            foreach (var e in CurrentRound.Events) e.ExecuteActions();
        }

        private void TriggerDecisions() {
            foreach (var d in CurrentRound.Decisions) d.Owner.AddDecision(d);
        }
        private void RollEvents() {
            var rand = new Random();
            foreach (var randEvent in randomEvents.Where(randEvent => randEvent.TimesItCanHappen != randEvent.TimesItHappened)
                         .Where(randEvent => rand.NextDouble() <= randEvent.Probability)) {
                randEvent.ExecuteActions();
                randEvent.TimesItHappened += 1;
            }
        }

        private void calculateResult()
        {
            this.Result = (Hotel.Popularity + (Hotel.Money / 1000) + Hotel.Rooms.Count * 100);
        }

        private void End(string message) {
            HasEnded = true;
            calculateResult();
            parentForm.endGame(message, Result);
        }

        private void ApplySettings() {
            // Game
            QuarterLength = int.Parse(GameSettings.json["quarterLength"]);
            allowedRoomsToBuyCount = int.Parse(GameSettings.json["allowedRoomsToBuyCount"]);
            numberRoomsDeluxBecomeAvailable = int.Parse(GameSettings.json["numberRoomsDeluxBecomeAvailable"]);

            // Hotel
            switch (GameSettings.Place) {
                default:
                case Place.None:
                case Place.Suburbs:
                    Hotel.Money = int.Parse(GameSettings.json["MoneySuburbs"]);
                    Hotel.MonthlyPayments = int.Parse(GameSettings.json["MonthlyPaymentsSuburbs"]);
                    Hotel.Popularity = int.Parse(GameSettings.json["PopularitySuburbs"]);
                    RoomDelux.CompetitorCost = int.Parse(GameSettings.json["CompetitorCostDeluxSuburbs"]);
                    RoomStandard.CompetitorCost = int.Parse(GameSettings.json["CompetitorCostStandardSuburbs"]);
                    break;
                case Place.MiddleCity:
                    Hotel.Money = int.Parse(GameSettings.json["MoneyMiddleCity"]);
                    Hotel.MonthlyPayments = int.Parse(GameSettings.json["MonthlyPaymentsMiddleCity"]);
                    Hotel.Popularity = int.Parse(GameSettings.json["PopularityMiddleCity"]);
                    RoomDelux.CompetitorCost = int.Parse(GameSettings.json["CompetitorCostDeluxMiddleCity"]);
                    RoomStandard.CompetitorCost = int.Parse(GameSettings.json["CompetitorCostStandardMiddleCity"]);
                    break;
                case Place.Center:
                    Hotel.Money = int.Parse(GameSettings.json["MoneyCenter"]);
                    Hotel.MonthlyPayments = int.Parse(GameSettings.json["MonthlyPaymentsCenter"]);
                    Hotel.Popularity = int.Parse(GameSettings.json["PopularityCenter"]);
                    RoomDelux.CompetitorCost = int.Parse(GameSettings.json["CompetitorCostDeluxCenter"]);
                    RoomStandard.CompetitorCost = int.Parse(GameSettings.json["CompetitorCostStandardCenter"]);
                    break;
            }

            // Bank
            Bank.AdditionalCreditPerRound = int.Parse(GameSettings.json["AdditionalCreditPerRound"]);
            Bank.InterestRate = double.Parse(GameSettings.json["InterestRate"]);
            Bank.InterestRateIncrease = double.Parse(GameSettings.json["InterestRateIncrease"]);
            Bank.MaxCredit = int.Parse(GameSettings.json["MaxCredit"]);

            // RoomStandard
            RoomStandard.IncomePerDay = int.Parse(GameSettings.json["RoomStandardIncomePerDay"]);
            RoomStandard.CostPerDay = int.Parse(GameSettings.json["RoomStandardCostPerDay"]);
            RoomStandard.BuyCost = int.Parse(GameSettings.json["RoomStandardBuyCost"]);

            // RoomDelux
            RoomDelux.IncomePerDay = int.Parse(GameSettings.json["RoomDeluxIncomePerDay"]);
            RoomDelux.CostPerDay = int.Parse(GameSettings.json["RoomDeluxCostPerDay"]);
            RoomDelux.BuyCost = int.Parse(GameSettings.json["RoomDeluxBuyCost"]);

            // Workers
            Workers.minimumWage = int.Parse(GameSettings.json["minimumWage"]);
            Workers.averageWage = double.Parse(GameSettings.json["averageWage"]);
            Workers.defaultMonthlyPayment = int.Parse(GameSettings.json["defaultMonthlyPayment"]);
            Workers.defaultHappiness = int.Parse(GameSettings.json["defaultHappiness"]);
        }

        private void LoadRounds() {
            AllRounds = Rounds.getRounds(this);
            CurrentRound = AllRounds[0];
        }
    }
}