namespace DecisionGame.BackEnd {
    public class RoomStandard : Room {
        /// <summary>
        /// Class representing standard rooms available from the start of the game. 
        /// </summary>
        ///     /// <description>
        /// It has room costs and income, separate from other types of rooms.
        /// </description>
        public static int IncomePerDay;
        public static int CostPerDay = 300;
        public static int BuyCost;
        public static int CompetitorCost;

        public override int GetIncomePerDay() {
            return IncomePerDay;
        }
        public override int GetCostPerDay() {
            return CostPerDay;
        }
        public override int GetBuyCost() {
            return BuyCost;
        }
        public static int StaticIncomePerDay() {
            return IncomePerDay;
        }
        public static int StaticCostPerDay() {
            return CostPerDay;
        }
        public static int StaticBuyCost() {
            return BuyCost;
        }
        public override int GetCompetitorCost()
        {
            return CompetitorCost;
        }
    }
}