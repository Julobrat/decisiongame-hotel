using System;
using System.Collections.Generic;
using DecisionGame.FrontEnd;

namespace DecisionGame.BackEnd {
        /// <summary>
        /// One of the classes representing bookmarks visible in game. 
        /// </summary>
        /// <description>
        /// In window connected to this class user can hire employees.
        /// </description>
    public class WorkOffice {
        private readonly Game parent;
        public Workers workers;

        public WorkOfficeForm workOfficeForm => parent.parentForm.workOfficeForm;

        public int MinimumSalary = 1000;
        public WorkOffice(Game parent) 
        {
            this.parent = parent;
            this.workers = new Workers();
        }

        public void PrepareForNextRound() {
            workOfficeForm.PrepareForNextRound();
        }

        public bool AreDecisionToMake() {
            foreach (var dec in workOfficeForm.decisions.Values)
                if (!dec.Completed && !dec.IsPermament)
                    return true;
            return false;
        }

        public void hireEmploye(Profession profession, int numberOfWorkers, int salary) {
            workers.HireWorkers(profession, numberOfWorkers);
            parent.Hotel.EmployeesCount += numberOfWorkers;
            parent.Hotel.MonthlyPayments += numberOfWorkers * salary;
        }
    }
}