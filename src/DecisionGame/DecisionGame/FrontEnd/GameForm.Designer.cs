﻿using System.ComponentModel;
using System.Windows.Forms;

namespace DecisionGame
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameForm));
            this.GameGroupBox = new System.Windows.Forms.GroupBox();
            this.MainMenuGroupBox = new System.Windows.Forms.GroupBox();
            this.CostsGroupBox = new System.Windows.Forms.GroupBox();
            this.CostsTitleLabel = new System.Windows.Forms.Label();
            this.CostsLabel = new System.Windows.Forms.Label();
            this.CostsPictureBox = new System.Windows.Forms.PictureBox();
            this.EmployeeCountGroupBox = new System.Windows.Forms.GroupBox();
            this.EmployeeCountTitleLabel = new System.Windows.Forms.Label();
            this.EmployeeCountLabel = new System.Windows.Forms.Label();
            this.EmployeeCountPictureBox = new System.Windows.Forms.PictureBox();
            this.IncomeGroupBox = new System.Windows.Forms.GroupBox();
            this.IncomeTitleLabel = new System.Windows.Forms.Label();
            this.IncomeLabel = new System.Windows.Forms.Label();
            this.IncomePictureBox = new System.Windows.Forms.PictureBox();
            this.DeluxCountGroupBox = new System.Windows.Forms.GroupBox();
            this.WorkTimeTitleLabel = new System.Windows.Forms.Label();
            this.DeluxCountLabel = new System.Windows.Forms.Label();
            this.MinimalSalaryGroupBox = new System.Windows.Forms.GroupBox();
            this.MinimalSalaryTitleLabel = new System.Windows.Forms.Label();
            this.MinimalSalaryLabel = new System.Windows.Forms.Label();
            this.MinimalSalaryPictureBox = new System.Windows.Forms.PictureBox();
            this.LogoGroupBox = new System.Windows.Forms.GroupBox();
            this.LogoTitleLabel = new System.Windows.Forms.Label();
            this.CompanyNameLabel = new System.Windows.Forms.Label();
            this.LogoPictureBox = new System.Windows.Forms.PictureBox();
            this.RoomCountGroupBox = new System.Windows.Forms.GroupBox();
            this.RoomCountTitleLabel = new System.Windows.Forms.Label();
            this.RoomCountLabel = new System.Windows.Forms.Label();
            this.RoomCountPictureBox = new System.Windows.Forms.PictureBox();
            this.MoneyGroupBox = new System.Windows.Forms.GroupBox();
            this.MoneyTitleLabel = new System.Windows.Forms.Label();
            this.LabelMoney = new System.Windows.Forms.Label();
            this.MoneyPictureBox = new System.Windows.Forms.PictureBox();
            this.QuarterLabel = new System.Windows.Forms.Label();
            this.NextRoundButton = new System.Windows.Forms.Button();
            this.LeftMenuGroupBox = new System.Windows.Forms.GroupBox();
            this.ReportButton = new System.Windows.Forms.Button();
            this.BuyRoomButton = new System.Windows.Forms.Button();
            this.DecisionLogButton = new System.Windows.Forms.Button();
            this.WorkOfficeButton = new System.Windows.Forms.Button();
            this.MessagesButton = new System.Windows.Forms.Button();
            this.AdAgencyButton = new System.Windows.Forms.Button();
            this.TownHallButton = new System.Windows.Forms.Button();
            this.BankButton = new System.Windows.Forms.Button();
            this.HotelButton = new System.Windows.Forms.Button();
            this.NextRoundProgessBar = new System.Windows.Forms.ProgressBar();
            this.pictureBoxDeluxCount = new System.Windows.Forms.PictureBox();
            this.GameGroupBox.SuspendLayout();
            this.MainMenuGroupBox.SuspendLayout();
            this.CostsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CostsPictureBox)).BeginInit();
            this.EmployeeCountGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeCountPictureBox)).BeginInit();
            this.IncomeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IncomePictureBox)).BeginInit();
            this.DeluxCountGroupBox.SuspendLayout();
            this.MinimalSalaryGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MinimalSalaryPictureBox)).BeginInit();
            this.LogoGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).BeginInit();
            this.RoomCountGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RoomCountPictureBox)).BeginInit();
            this.MoneyGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MoneyPictureBox)).BeginInit();
            this.LeftMenuGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDeluxCount)).BeginInit();
            this.SuspendLayout();
            // 
            // GameGroupBox
            // 
            this.GameGroupBox.Controls.Add(this.MainMenuGroupBox);
            this.GameGroupBox.Controls.Add(this.NextRoundButton);
            this.GameGroupBox.Controls.Add(this.LeftMenuGroupBox);
            this.GameGroupBox.Controls.Add(this.NextRoundProgessBar);
            this.GameGroupBox.Location = new System.Drawing.Point(0, 0);
            this.GameGroupBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.GameGroupBox.Name = "GameGroupBox";
            this.GameGroupBox.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.GameGroupBox.Size = new System.Drawing.Size(1080, 720);
            this.GameGroupBox.TabIndex = 2;
            this.GameGroupBox.TabStop = false;
            // 
            // MainMenuGroupBox
            // 
            this.MainMenuGroupBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MainMenuGroupBox.Controls.Add(this.CostsGroupBox);
            this.MainMenuGroupBox.Controls.Add(this.EmployeeCountGroupBox);
            this.MainMenuGroupBox.Controls.Add(this.IncomeGroupBox);
            this.MainMenuGroupBox.Controls.Add(this.DeluxCountGroupBox);
            this.MainMenuGroupBox.Controls.Add(this.MinimalSalaryGroupBox);
            this.MainMenuGroupBox.Controls.Add(this.LogoGroupBox);
            this.MainMenuGroupBox.Controls.Add(this.RoomCountGroupBox);
            this.MainMenuGroupBox.Controls.Add(this.MoneyGroupBox);
            this.MainMenuGroupBox.Controls.Add(this.QuarterLabel);
            this.MainMenuGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuGroupBox.Location = new System.Drawing.Point(121, 12);
            this.MainMenuGroupBox.Name = "MainMenuGroupBox";
            this.MainMenuGroupBox.Size = new System.Drawing.Size(803, 540);
            this.MainMenuGroupBox.TabIndex = 5;
            this.MainMenuGroupBox.TabStop = false;
            this.MainMenuGroupBox.Text = "Informacje i Statystyki";
            // 
            // CostsGroupBox
            // 
            this.CostsGroupBox.Controls.Add(this.CostsTitleLabel);
            this.CostsGroupBox.Controls.Add(this.CostsLabel);
            this.CostsGroupBox.Controls.Add(this.CostsPictureBox);
            this.CostsGroupBox.Location = new System.Drawing.Point(614, 310);
            this.CostsGroupBox.Name = "CostsGroupBox";
            this.CostsGroupBox.Size = new System.Drawing.Size(175, 152);
            this.CostsGroupBox.TabIndex = 12;
            this.CostsGroupBox.TabStop = false;
            // 
            // CostsTitleLabel
            // 
            this.CostsTitleLabel.AutoSize = true;
            this.CostsTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CostsTitleLabel.Location = new System.Drawing.Point(6, 0);
            this.CostsTitleLabel.Name = "CostsTitleLabel";
            this.CostsTitleLabel.Size = new System.Drawing.Size(156, 20);
            this.CostsTitleLabel.TabIndex = 4;
            this.CostsTitleLabel.Text = "Koszty miesięczne";
            // 
            // CostsLabel
            // 
            this.CostsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CostsLabel.Location = new System.Drawing.Point(16, 121);
            this.CostsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CostsLabel.Name = "CostsLabel";
            this.CostsLabel.Size = new System.Drawing.Size(141, 19);
            this.CostsLabel.TabIndex = 0;
            this.CostsLabel.Text = "0 PLN";
            this.CostsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CostsPictureBox
            // 
            this.CostsPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CostsPictureBox.BackgroundImage")));
            this.CostsPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CostsPictureBox.InitialImage = null;
            this.CostsPictureBox.Location = new System.Drawing.Point(20, 27);
            this.CostsPictureBox.Name = "CostsPictureBox";
            this.CostsPictureBox.Size = new System.Drawing.Size(135, 91);
            this.CostsPictureBox.TabIndex = 5;
            this.CostsPictureBox.TabStop = false;
            // 
            // EmployeeCountGroupBox
            // 
            this.EmployeeCountGroupBox.Controls.Add(this.EmployeeCountTitleLabel);
            this.EmployeeCountGroupBox.Controls.Add(this.EmployeeCountLabel);
            this.EmployeeCountGroupBox.Controls.Add(this.EmployeeCountPictureBox);
            this.EmployeeCountGroupBox.Location = new System.Drawing.Point(421, 310);
            this.EmployeeCountGroupBox.Name = "EmployeeCountGroupBox";
            this.EmployeeCountGroupBox.Size = new System.Drawing.Size(175, 152);
            this.EmployeeCountGroupBox.TabIndex = 11;
            this.EmployeeCountGroupBox.TabStop = false;
            // 
            // EmployeeCountTitleLabel
            // 
            this.EmployeeCountTitleLabel.AutoSize = true;
            this.EmployeeCountTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmployeeCountTitleLabel.Location = new System.Drawing.Point(12, 4);
            this.EmployeeCountTitleLabel.Name = "EmployeeCountTitleLabel";
            this.EmployeeCountTitleLabel.Size = new System.Drawing.Size(154, 20);
            this.EmployeeCountTitleLabel.TabIndex = 4;
            this.EmployeeCountTitleLabel.Text = "Ilość pracowników";
            // 
            // EmployeeCountLabel
            // 
            this.EmployeeCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmployeeCountLabel.Location = new System.Drawing.Point(16, 121);
            this.EmployeeCountLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.EmployeeCountLabel.Name = "EmployeeCountLabel";
            this.EmployeeCountLabel.Size = new System.Drawing.Size(141, 19);
            this.EmployeeCountLabel.TabIndex = 0;
            this.EmployeeCountLabel.Text = "0";
            this.EmployeeCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EmployeeCountPictureBox
            // 
            this.EmployeeCountPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("EmployeeCountPictureBox.BackgroundImage")));
            this.EmployeeCountPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.EmployeeCountPictureBox.InitialImage = null;
            this.EmployeeCountPictureBox.Location = new System.Drawing.Point(20, 27);
            this.EmployeeCountPictureBox.Name = "EmployeeCountPictureBox";
            this.EmployeeCountPictureBox.Size = new System.Drawing.Size(135, 91);
            this.EmployeeCountPictureBox.TabIndex = 5;
            this.EmployeeCountPictureBox.TabStop = false;
            // 
            // IncomeGroupBox
            // 
            this.IncomeGroupBox.Controls.Add(this.IncomeTitleLabel);
            this.IncomeGroupBox.Controls.Add(this.IncomeLabel);
            this.IncomeGroupBox.Controls.Add(this.IncomePictureBox);
            this.IncomeGroupBox.Location = new System.Drawing.Point(21, 310);
            this.IncomeGroupBox.Name = "IncomeGroupBox";
            this.IncomeGroupBox.Size = new System.Drawing.Size(175, 152);
            this.IncomeGroupBox.TabIndex = 10;
            this.IncomeGroupBox.TabStop = false;
            // 
            // IncomeTitleLabel
            // 
            this.IncomeTitleLabel.AutoSize = true;
            this.IncomeTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IncomeTitleLabel.Location = new System.Drawing.Point(16, 4);
            this.IncomeTitleLabel.Name = "IncomeTitleLabel";
            this.IncomeTitleLabel.Size = new System.Drawing.Size(152, 20);
            this.IncomeTitleLabel.TabIndex = 4;
            this.IncomeTitleLabel.Text = "Dochód w rundzie";
            // 
            // IncomeLabel
            // 
            this.IncomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IncomeLabel.Location = new System.Drawing.Point(16, 121);
            this.IncomeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IncomeLabel.Name = "IncomeLabel";
            this.IncomeLabel.Size = new System.Drawing.Size(141, 19);
            this.IncomeLabel.TabIndex = 0;
            this.IncomeLabel.Text = "0 PLN";
            this.IncomeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // IncomePictureBox
            // 
            this.IncomePictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("IncomePictureBox.BackgroundImage")));
            this.IncomePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.IncomePictureBox.InitialImage = null;
            this.IncomePictureBox.Location = new System.Drawing.Point(20, 27);
            this.IncomePictureBox.Name = "IncomePictureBox";
            this.IncomePictureBox.Size = new System.Drawing.Size(135, 91);
            this.IncomePictureBox.TabIndex = 5;
            this.IncomePictureBox.TabStop = false;
            // 
            // DeluxCountGroupBox
            // 
            this.DeluxCountGroupBox.Controls.Add(this.pictureBoxDeluxCount);
            this.DeluxCountGroupBox.Controls.Add(this.WorkTimeTitleLabel);
            this.DeluxCountGroupBox.Controls.Add(this.DeluxCountLabel);
            this.DeluxCountGroupBox.Location = new System.Drawing.Point(220, 314);
            this.DeluxCountGroupBox.Name = "DeluxCountGroupBox";
            this.DeluxCountGroupBox.Size = new System.Drawing.Size(175, 152);
            this.DeluxCountGroupBox.TabIndex = 9;
            this.DeluxCountGroupBox.TabStop = false;
            // 
            // WorkTimeTitleLabel
            // 
            this.WorkTimeTitleLabel.AutoSize = true;
            this.WorkTimeTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WorkTimeTitleLabel.Location = new System.Drawing.Point(22, 0);
            this.WorkTimeTitleLabel.Name = "WorkTimeTitleLabel";
            this.WorkTimeTitleLabel.Size = new System.Drawing.Size(135, 18);
            this.WorkTimeTitleLabel.TabIndex = 4;
            this.WorkTimeTitleLabel.Text = "Ilość pokoi delux";
            // 
            // DeluxCountLabel
            // 
            this.DeluxCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeluxCountLabel.Location = new System.Drawing.Point(16, 121);
            this.DeluxCountLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.DeluxCountLabel.Name = "DeluxCountLabel";
            this.DeluxCountLabel.Size = new System.Drawing.Size(141, 19);
            this.DeluxCountLabel.TabIndex = 0;
            this.DeluxCountLabel.Text = "0";
            this.DeluxCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MinimalSalaryGroupBox
            // 
            this.MinimalSalaryGroupBox.Controls.Add(this.MinimalSalaryTitleLabel);
            this.MinimalSalaryGroupBox.Controls.Add(this.MinimalSalaryLabel);
            this.MinimalSalaryGroupBox.Controls.Add(this.MinimalSalaryPictureBox);
            this.MinimalSalaryGroupBox.Location = new System.Drawing.Point(614, 106);
            this.MinimalSalaryGroupBox.Name = "MinimalSalaryGroupBox";
            this.MinimalSalaryGroupBox.Size = new System.Drawing.Size(175, 152);
            this.MinimalSalaryGroupBox.TabIndex = 9;
            this.MinimalSalaryGroupBox.TabStop = false;
            // 
            // MinimalSalaryTitleLabel
            // 
            this.MinimalSalaryTitleLabel.AutoSize = true;
            this.MinimalSalaryTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimalSalaryTitleLabel.Location = new System.Drawing.Point(6, 4);
            this.MinimalSalaryTitleLabel.Name = "MinimalSalaryTitleLabel";
            this.MinimalSalaryTitleLabel.Size = new System.Drawing.Size(175, 15);
            this.MinimalSalaryTitleLabel.TabIndex = 4;
            this.MinimalSalaryTitleLabel.Text = "Minimalne wynagrodzenie";
            // 
            // MinimalSalaryLabel
            // 
            this.MinimalSalaryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimalSalaryLabel.Location = new System.Drawing.Point(16, 121);
            this.MinimalSalaryLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.MinimalSalaryLabel.Name = "MinimalSalaryLabel";
            this.MinimalSalaryLabel.Size = new System.Drawing.Size(141, 19);
            this.MinimalSalaryLabel.TabIndex = 0;
            this.MinimalSalaryLabel.Text = "1000 PLN / miesiąc";
            this.MinimalSalaryLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MinimalSalaryPictureBox
            // 
            this.MinimalSalaryPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MinimalSalaryPictureBox.BackgroundImage")));
            this.MinimalSalaryPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MinimalSalaryPictureBox.InitialImage = null;
            this.MinimalSalaryPictureBox.Location = new System.Drawing.Point(20, 27);
            this.MinimalSalaryPictureBox.Name = "MinimalSalaryPictureBox";
            this.MinimalSalaryPictureBox.Size = new System.Drawing.Size(135, 91);
            this.MinimalSalaryPictureBox.TabIndex = 5;
            this.MinimalSalaryPictureBox.TabStop = false;
            // 
            // LogoGroupBox
            // 
            this.LogoGroupBox.Controls.Add(this.LogoTitleLabel);
            this.LogoGroupBox.Controls.Add(this.CompanyNameLabel);
            this.LogoGroupBox.Controls.Add(this.LogoPictureBox);
            this.LogoGroupBox.Location = new System.Drawing.Point(421, 106);
            this.LogoGroupBox.Name = "LogoGroupBox";
            this.LogoGroupBox.Size = new System.Drawing.Size(175, 152);
            this.LogoGroupBox.TabIndex = 8;
            this.LogoGroupBox.TabStop = false;
            // 
            // LogoTitleLabel
            // 
            this.LogoTitleLabel.AutoSize = true;
            this.LogoTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LogoTitleLabel.Location = new System.Drawing.Point(12, 4);
            this.LogoTitleLabel.Name = "LogoTitleLabel";
            this.LogoTitleLabel.Size = new System.Drawing.Size(157, 20);
            this.LogoTitleLabel.TabIndex = 4;
            this.LogoTitleLabel.Text = "Logo i nazwa firmy";
            // 
            // CompanyNameLabel
            // 
            this.CompanyNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CompanyNameLabel.Location = new System.Drawing.Point(16, 121);
            this.CompanyNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CompanyNameLabel.Name = "CompanyNameLabel";
            this.CompanyNameLabel.Size = new System.Drawing.Size(141, 19);
            this.CompanyNameLabel.TabIndex = 0;
            this.CompanyNameLabel.Text = "Belweder";
            this.CompanyNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LogoPictureBox
            // 
            this.LogoPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("LogoPictureBox.BackgroundImage")));
            this.LogoPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LogoPictureBox.InitialImage = null;
            this.LogoPictureBox.Location = new System.Drawing.Point(20, 27);
            this.LogoPictureBox.Name = "LogoPictureBox";
            this.LogoPictureBox.Size = new System.Drawing.Size(135, 91);
            this.LogoPictureBox.TabIndex = 5;
            this.LogoPictureBox.TabStop = false;
            // 
            // RoomCountGroupBox
            // 
            this.RoomCountGroupBox.Controls.Add(this.RoomCountTitleLabel);
            this.RoomCountGroupBox.Controls.Add(this.RoomCountLabel);
            this.RoomCountGroupBox.Controls.Add(this.RoomCountPictureBox);
            this.RoomCountGroupBox.Location = new System.Drawing.Point(220, 106);
            this.RoomCountGroupBox.Name = "RoomCountGroupBox";
            this.RoomCountGroupBox.Size = new System.Drawing.Size(175, 152);
            this.RoomCountGroupBox.TabIndex = 7;
            this.RoomCountGroupBox.TabStop = false;
            // 
            // RoomCountTitleLabel
            // 
            this.RoomCountTitleLabel.AutoSize = true;
            this.RoomCountTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RoomCountTitleLabel.Location = new System.Drawing.Point(40, 4);
            this.RoomCountTitleLabel.Name = "RoomCountTitleLabel";
            this.RoomCountTitleLabel.Size = new System.Drawing.Size(95, 20);
            this.RoomCountTitleLabel.TabIndex = 4;
            this.RoomCountTitleLabel.Text = "Ilość pokoi";
            // 
            // RoomCountLabel
            // 
            this.RoomCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RoomCountLabel.Location = new System.Drawing.Point(16, 121);
            this.RoomCountLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.RoomCountLabel.Name = "RoomCountLabel";
            this.RoomCountLabel.Size = new System.Drawing.Size(141, 19);
            this.RoomCountLabel.TabIndex = 0;
            this.RoomCountLabel.Text = "0";
            this.RoomCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RoomCountPictureBox
            // 
            this.RoomCountPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("RoomCountPictureBox.BackgroundImage")));
            this.RoomCountPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RoomCountPictureBox.InitialImage = null;
            this.RoomCountPictureBox.Location = new System.Drawing.Point(20, 27);
            this.RoomCountPictureBox.Name = "RoomCountPictureBox";
            this.RoomCountPictureBox.Size = new System.Drawing.Size(135, 91);
            this.RoomCountPictureBox.TabIndex = 5;
            this.RoomCountPictureBox.TabStop = false;
            // 
            // MoneyGroupBox
            // 
            this.MoneyGroupBox.Controls.Add(this.MoneyTitleLabel);
            this.MoneyGroupBox.Controls.Add(this.LabelMoney);
            this.MoneyGroupBox.Controls.Add(this.MoneyPictureBox);
            this.MoneyGroupBox.Location = new System.Drawing.Point(21, 106);
            this.MoneyGroupBox.Name = "MoneyGroupBox";
            this.MoneyGroupBox.Size = new System.Drawing.Size(175, 152);
            this.MoneyGroupBox.TabIndex = 6;
            this.MoneyGroupBox.TabStop = false;
            // 
            // MoneyTitleLabel
            // 
            this.MoneyTitleLabel.AutoSize = true;
            this.MoneyTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MoneyTitleLabel.Location = new System.Drawing.Point(40, 4);
            this.MoneyTitleLabel.Name = "MoneyTitleLabel";
            this.MoneyTitleLabel.Size = new System.Drawing.Size(105, 20);
            this.MoneyTitleLabel.TabIndex = 4;
            this.MoneyTitleLabel.Text = "Saldo konta";
            // 
            // LabelMoney
            // 
            this.LabelMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMoney.Location = new System.Drawing.Point(16, 121);
            this.LabelMoney.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LabelMoney.Name = "LabelMoney";
            this.LabelMoney.Size = new System.Drawing.Size(141, 19);
            this.LabelMoney.TabIndex = 0;
            this.LabelMoney.Text = "0 PLN";
            this.LabelMoney.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MoneyPictureBox
            // 
            this.MoneyPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MoneyPictureBox.BackgroundImage")));
            this.MoneyPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MoneyPictureBox.InitialImage = null;
            this.MoneyPictureBox.Location = new System.Drawing.Point(20, 27);
            this.MoneyPictureBox.Name = "MoneyPictureBox";
            this.MoneyPictureBox.Size = new System.Drawing.Size(135, 91);
            this.MoneyPictureBox.TabIndex = 5;
            this.MoneyPictureBox.TabStop = false;
            // 
            // QuarterLabel
            // 
            this.QuarterLabel.AutoSize = true;
            this.QuarterLabel.Font = new System.Drawing.Font("MV Boli", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuarterLabel.Location = new System.Drawing.Point(348, 16);
            this.QuarterLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.QuarterLabel.Name = "QuarterLabel";
            this.QuarterLabel.Size = new System.Drawing.Size(130, 29);
            this.QuarterLabel.TabIndex = 2;
            this.QuarterLabel.Text = "Kwartał: 1";
            // 
            // NextRoundButton
            // 
            this.NextRoundButton.BackgroundImage = global::DecisionGame.Properties.Resources.btn_green;
            this.NextRoundButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.NextRoundButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NextRoundButton.Location = new System.Drawing.Point(445, 557);
            this.NextRoundButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.NextRoundButton.Name = "NextRoundButton";
            this.NextRoundButton.Size = new System.Drawing.Size(162, 32);
            this.NextRoundButton.TabIndex = 1;
            this.NextRoundButton.Text = "Nastepny Kwartał";
            this.NextRoundButton.UseVisualStyleBackColor = true;
            this.NextRoundButton.Click += new System.EventHandler(this.NextRoundButton_Click);
            // 
            // LeftMenuGroupBox
            // 
            this.LeftMenuGroupBox.Controls.Add(this.ReportButton);
            this.LeftMenuGroupBox.Controls.Add(this.BuyRoomButton);
            this.LeftMenuGroupBox.Controls.Add(this.DecisionLogButton);
            this.LeftMenuGroupBox.Controls.Add(this.WorkOfficeButton);
            this.LeftMenuGroupBox.Controls.Add(this.MessagesButton);
            this.LeftMenuGroupBox.Controls.Add(this.AdAgencyButton);
            this.LeftMenuGroupBox.Controls.Add(this.TownHallButton);
            this.LeftMenuGroupBox.Controls.Add(this.BankButton);
            this.LeftMenuGroupBox.Controls.Add(this.HotelButton);
            this.LeftMenuGroupBox.Location = new System.Drawing.Point(0, 0);
            this.LeftMenuGroupBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.LeftMenuGroupBox.Name = "LeftMenuGroupBox";
            this.LeftMenuGroupBox.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.LeftMenuGroupBox.Size = new System.Drawing.Size(120, 590);
            this.LeftMenuGroupBox.TabIndex = 0;
            this.LeftMenuGroupBox.TabStop = false;
            // 
            // ReportButton
            // 
            this.ReportButton.BackgroundImage = global::DecisionGame.Properties.Resources.btn_green;
            this.ReportButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ReportButton.Location = new System.Drawing.Point(4, 514);
            this.ReportButton.Name = "ReportButton";
            this.ReportButton.Size = new System.Drawing.Size(109, 23);
            this.ReportButton.TabIndex = 12;
            this.ReportButton.Text = "Raporty";
            this.ReportButton.UseVisualStyleBackColor = true;
            this.ReportButton.Visible = false;
            // 
            // BuyRoomButton
            // 
            this.BuyRoomButton.BackgroundImage = global::DecisionGame.Properties.Resources.btn_yellow;
            this.BuyRoomButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BuyRoomButton.Location = new System.Drawing.Point(4, 256);
            this.BuyRoomButton.Name = "BuyRoomButton";
            this.BuyRoomButton.Size = new System.Drawing.Size(109, 23);
            this.BuyRoomButton.TabIndex = 9;
            this.BuyRoomButton.Text = "Kup Pokój";
            this.BuyRoomButton.UseVisualStyleBackColor = true;
            this.BuyRoomButton.Click += new System.EventHandler(this.BuyRoomButton_Click);
            // 
            // DecisionLogButton
            // 
            this.DecisionLogButton.BackgroundImage = global::DecisionGame.Properties.Resources.btn_green;
            this.DecisionLogButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DecisionLogButton.Location = new System.Drawing.Point(4, 472);
            this.DecisionLogButton.Name = "DecisionLogButton";
            this.DecisionLogButton.Size = new System.Drawing.Size(109, 23);
            this.DecisionLogButton.TabIndex = 11;
            this.DecisionLogButton.Text = "Log decyzji";
            this.DecisionLogButton.UseVisualStyleBackColor = true;
            this.DecisionLogButton.Visible = false;
            // 
            // WorkOfficeButton
            // 
            this.WorkOfficeButton.BackgroundImage = global::DecisionGame.Properties.Resources.btn_orange;
            this.WorkOfficeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.WorkOfficeButton.Location = new System.Drawing.Point(4, 219);
            this.WorkOfficeButton.Name = "WorkOfficeButton";
            this.WorkOfficeButton.Size = new System.Drawing.Size(109, 23);
            this.WorkOfficeButton.TabIndex = 8;
            this.WorkOfficeButton.Text = "Urząd Pracy";
            this.WorkOfficeButton.UseVisualStyleBackColor = true;
            this.WorkOfficeButton.Click += new System.EventHandler(this.WorkOfficeButton_Click);
            // 
            // MessagesButton
            // 
            this.MessagesButton.BackgroundImage = global::DecisionGame.Properties.Resources.btn_green;
            this.MessagesButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MessagesButton.Location = new System.Drawing.Point(4, 429);
            this.MessagesButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MessagesButton.Name = "MessagesButton";
            this.MessagesButton.Size = new System.Drawing.Size(109, 24);
            this.MessagesButton.TabIndex = 10;
            this.MessagesButton.Text = "Wiadomości";
            this.MessagesButton.UseVisualStyleBackColor = true;
            this.MessagesButton.Visible = false;
            // 
            // AdAgencyButton
            // 
            this.AdAgencyButton.BackgroundImage = global::DecisionGame.Properties.Resources.btn_cyan;
            this.AdAgencyButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AdAgencyButton.Location = new System.Drawing.Point(4, 178);
            this.AdAgencyButton.Name = "AdAgencyButton";
            this.AdAgencyButton.Size = new System.Drawing.Size(109, 23);
            this.AdAgencyButton.TabIndex = 7;
            this.AdAgencyButton.Text = "Agencja Reklamy";
            this.AdAgencyButton.UseVisualStyleBackColor = true;
            this.AdAgencyButton.Click += new System.EventHandler(this.AdAgencyButton_Click);
            // 
            // TownHallButton
            // 
            this.TownHallButton.BackgroundImage = global::DecisionGame.Properties.Resources.btn_green;
            this.TownHallButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TownHallButton.Location = new System.Drawing.Point(4, 134);
            this.TownHallButton.Name = "TownHallButton";
            this.TownHallButton.Size = new System.Drawing.Size(109, 23);
            this.TownHallButton.TabIndex = 6;
            this.TownHallButton.Text = "Ratusz";
            this.TownHallButton.UseVisualStyleBackColor = true;
            this.TownHallButton.Visible = false;
            // 
            // BankButton
            // 
            this.BankButton.BackgroundImage = global::DecisionGame.Properties.Resources.btn_orange;
            this.BankButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BankButton.Location = new System.Drawing.Point(4, 105);
            this.BankButton.Name = "BankButton";
            this.BankButton.Size = new System.Drawing.Size(109, 23);
            this.BankButton.TabIndex = 5;
            this.BankButton.Text = "Bank";
            this.BankButton.UseVisualStyleBackColor = true;
            this.BankButton.Click += new System.EventHandler(this.BankButton_Click);
            // 
            // HotelButton
            // 
            this.HotelButton.BackgroundImage = global::DecisionGame.Properties.Resources.btn_yellow;
            this.HotelButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HotelButton.Location = new System.Drawing.Point(4, 46);
            this.HotelButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.HotelButton.Name = "HotelButton";
            this.HotelButton.Size = new System.Drawing.Size(109, 68);
            this.HotelButton.TabIndex = 4;
            this.HotelButton.Text = "Hotel";
            this.HotelButton.UseVisualStyleBackColor = true;
            this.HotelButton.Click += new System.EventHandler(this.HotelButton_Click);
            // 
            // NextRoundProgessBar
            // 
            this.NextRoundProgessBar.Location = new System.Drawing.Point(124, 557);
            this.NextRoundProgessBar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.NextRoundProgessBar.Name = "NextRoundProgessBar";
            this.NextRoundProgessBar.Size = new System.Drawing.Size(800, 33);
            this.NextRoundProgessBar.TabIndex = 3;
            // 
            // pictureBoxDeluxCount
            // 
            this.pictureBoxDeluxCount.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxDeluxCount.BackgroundImage")));
            this.pictureBoxDeluxCount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxDeluxCount.InitialImage = null;
            this.pictureBoxDeluxCount.Location = new System.Drawing.Point(20, 21);
            this.pictureBoxDeluxCount.Name = "pictureBoxDeluxCount";
            this.pictureBoxDeluxCount.Size = new System.Drawing.Size(135, 91);
            this.pictureBoxDeluxCount.TabIndex = 6;
            this.pictureBoxDeluxCount.TabStop = false;
            // 
            // GameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 612);
            this.Controls.Add(this.GameGroupBox);
            this.Name = "GameForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hotel Stars";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GameForm_FormClosed);
            this.GameGroupBox.ResumeLayout(false);
            this.MainMenuGroupBox.ResumeLayout(false);
            this.MainMenuGroupBox.PerformLayout();
            this.CostsGroupBox.ResumeLayout(false);
            this.CostsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CostsPictureBox)).EndInit();
            this.EmployeeCountGroupBox.ResumeLayout(false);
            this.EmployeeCountGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeCountPictureBox)).EndInit();
            this.IncomeGroupBox.ResumeLayout(false);
            this.IncomeGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IncomePictureBox)).EndInit();
            this.DeluxCountGroupBox.ResumeLayout(false);
            this.DeluxCountGroupBox.PerformLayout();
            this.MinimalSalaryGroupBox.ResumeLayout(false);
            this.MinimalSalaryGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MinimalSalaryPictureBox)).EndInit();
            this.LogoGroupBox.ResumeLayout(false);
            this.LogoGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).EndInit();
            this.RoomCountGroupBox.ResumeLayout(false);
            this.RoomCountGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RoomCountPictureBox)).EndInit();
            this.MoneyGroupBox.ResumeLayout(false);
            this.MoneyGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MoneyPictureBox)).EndInit();
            this.LeftMenuGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDeluxCount)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion
        private Label LabelMoney;
        private System.Windows.Forms.GroupBox GameGroupBox;
        private System.Windows.Forms.GroupBox LeftMenuGroupBox;
        private System.Windows.Forms.Button HotelButton;
        private System.Windows.Forms.Button NextRoundButton;
        private System.Windows.Forms.Label QuarterLabel;
        private ProgressBar NextRoundProgessBar;
        private System.Windows.Forms.Button BankButton;
        private System.Windows.Forms.Button TownHallButton;
        private System.Windows.Forms.Button WorkOfficeButton;
        private System.Windows.Forms.Button AdAgencyButton;
        private System.Windows.Forms.Button BuyRoomButton;
        private System.Windows.Forms.Button ReportButton;
        private System.Windows.Forms.Button DecisionLogButton;
        private System.Windows.Forms.Button MessagesButton;
        private System.Windows.Forms.GroupBox MainMenuGroupBox;
        private Label MoneyTitleLabel;
        private PictureBox MoneyPictureBox;
        private GroupBox MoneyGroupBox;
        private GroupBox RoomCountGroupBox;
        private Label RoomCountTitleLabel;
        private Label RoomCountLabel;
        private PictureBox RoomCountPictureBox;
        private GroupBox LogoGroupBox;
        private Label LogoTitleLabel;
        private Label CompanyNameLabel;
        private PictureBox LogoPictureBox;
        private GroupBox MinimalSalaryGroupBox;
        private Label MinimalSalaryTitleLabel;
        private Label MinimalSalaryLabel;
        private PictureBox MinimalSalaryPictureBox;
        private GroupBox DeluxCountGroupBox;
        private Label WorkTimeTitleLabel;
        private Label DeluxCountLabel;
        private GroupBox EmployeeCountGroupBox;
        private Label EmployeeCountTitleLabel;
        private Label EmployeeCountLabel;
        private PictureBox EmployeeCountPictureBox;
        private GroupBox IncomeGroupBox;
        private Label IncomeTitleLabel;
        private Label IncomeLabel;
        private PictureBox IncomePictureBox;
        private GroupBox CostsGroupBox;
        private Label CostsTitleLabel;
        private Label CostsLabel;
        private PictureBox CostsPictureBox;
        private PictureBox pictureBoxDeluxCount;
    }
}