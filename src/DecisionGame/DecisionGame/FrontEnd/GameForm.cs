﻿using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DecisionGame.BackEnd;
using DecisionGame.FrontEnd;

namespace DecisionGame {
    public partial class GameForm : Form {
        public AdAgencyForm adAgencyForm;
        public BankForm bankForm;
        public BuyRoomForm buyRoomForm;
        public HotelForm hotelForm;
        public WorkOfficeForm workOfficeForm;

        public Game Game;

        private int roomCount;
        private int roomDeluxCount;

        public GameForm(InitialSettings settings, Image img) {
            InitializeComponent();

            StartPosition = FormStartPosition.Manual;

            Location = new Point(
                (Screen.FromControl(this).WorkingArea.Width - Width) / 2,
                (Screen.FromControl(this).WorkingArea.Height - Height) / 2
            );

            adAgencyForm = new AdAgencyForm(this);
            bankForm = new BankForm(this);
            hotelForm = new HotelForm(this);
            workOfficeForm = new WorkOfficeForm(this);

            Game = new Game(this, settings);

            RoomCount = Game.Hotel.Rooms.Count();

            GameGroupBox.Size = new Size(ClientSize.Width, ClientSize.Height);

            var heightBetweenButtons = 20;

            BankButton.Size = HotelButton.Size;
            BankButton.Location = new Point(HotelButton.Location.X,
                HotelButton.Location.Y + HotelButton.Height + heightBetweenButtons);

            /* TownHallButton.Size = HotelButton.Size;
             TownHallButton.Location = new Point(BankButton.Location.X,
                 BankButton.Location.Y + BankButton.Height + heightBetweenButtons);*/

            AdAgencyButton.Size = HotelButton.Size;
            AdAgencyButton.Location = new Point(BankButton.Location.X,
                BankButton.Location.Y + BankButton.Height + heightBetweenButtons);

            WorkOfficeButton.Size = HotelButton.Size;
            WorkOfficeButton.Location = new Point(AdAgencyButton.Location.X,
                AdAgencyButton.Location.Y + AdAgencyButton.Height + heightBetweenButtons);

            BuyRoomButton.Size = HotelButton.Size;
            BuyRoomButton.Location = new Point(WorkOfficeButton.Location.X,
                WorkOfficeButton.Location.Y + WorkOfficeButton.Height + heightBetweenButtons);

            LeftMenuGroupBox.Size = new Size(AdAgencyButton.Width + 10, ClientSize.Height);

            /*DecisionLogButton.Size = MessagesButton.Size;
            DecisionLogButton.Location = new Point(MessagesButton.Location.X,
                MessagesButton.Location.Y + MessagesButton.Height + heightBetweenButtons);

            ReportButton.Size = MessagesButton.Size;
            ReportButton.Location = new Point(DecisionLogButton.Location.X,
                DecisionLogButton.Location.Y + DecisionLogButton.Height + heightBetweenButtons);*/


            NextRoundButton.Location = new Point(
                (MainMenuGroupBox.Width + 10 - NextRoundButton.Width) / 2 + LeftMenuGroupBox.Width,
                GameGroupBox.Height - NextRoundButton.Height - 5);

            QuarterLabel.Location = new Point((QuarterLabel.Parent.Width - QuarterLabel.Width) / 2, heightBetweenButtons);

            NextRoundProgessBar.Size = new Size(MainMenuGroupBox.Width + 10, 30);
            NextRoundProgessBar.Location = new Point(LeftMenuGroupBox.Width, GameGroupBox.Height - NextRoundProgessBar.Height);

            MainMenuGroupBox.Size = new Size(ClientSize.Width - LeftMenuGroupBox.Width,
                ClientSize.Height - NextRoundProgessBar.Height);
            MainMenuGroupBox.Location = new Point(LeftMenuGroupBox.Width);
            MainMenuGroupBox.BackgroundImage = img;

            LabelMoney.AutoSize = true;
            LabelMoney.Location = new Point(0, MoneyPictureBox.Height + MoneyPictureBox.Location.Y);
            LabelMoney.AutoSize = false;
            LabelMoney.Size = new Size(LabelMoney.Parent.Width, LabelMoney.Height);

            buyRoomForm = new BuyRoomForm(this);

            CompanyNameLabel.Text = settings.PlayerName;

            Show();

            Game.Start();
        }

        public int RoomCount {
            get => roomCount;
            set {
                roomCount = value;
                RoomCountLabel.Text = value.ToString();
            }
        }

        public int RoomDeluxCount
        {
            get => roomDeluxCount;
            set
            {
                roomDeluxCount = value;
                DeluxCountLabel.Text = value.ToString();
            }
        }

        private async void NextRoundButton_Click(object sender, EventArgs e) {
            if (Game.AreDecisionsToMake()) {
                var result = MessageBox.Show("Są jeszcze decyzje do podjęcia, jeśli kontynuujesz, zostaną one podjęte automatycznie" +
                                             ", niektóre mogą przepaść bezpowrotnie. Czy chcesz kontynuować?",
                    "Niepodjęte decyzje", MessageBoxButtons.YesNo);
                if (result == DialogResult.No) return;
            }

            ((Button) sender).Enabled = false;

            NextRoundProgessBar.Increment(100);
            Game.AdvanceToNextRound();
            QuarterLabel.Text = "Kwartał: " + Game.CurrentRound.RoundNumber;

            if (!Game.HasEnded) {
                await Task.Delay(750);
                NextRoundProgessBar.Value = 0;

                ((Button) sender).Enabled = true;
            }
            else {
                NextRoundProgessBar.Value = 0;
            }
        }

        public void backToStandardLocation() {
            Location = new Point(
                (Screen.FromControl(this).WorkingArea.Width - Width) / 2,
                (Screen.FromControl(this).WorkingArea.Height - Height) / 2
            );
        }

        public void endGame(string message, double result) {
            GameGroupBox.Enabled = false;
            EndGame endGame = new EndGame(result, message);
            endGame.Show();
        }

        public void ChangedMoneyEvent() {
            LabelMoney.Text = Game.Hotel.Money + " PLN";
        }

        public void ChangedMinimalSalaryEvent(int newValue) {
            MinimalSalaryLabel.Text = newValue + " PLN / miesiąc";
        }

        public void ChangedCompanyNameEvent(string newValue) {
            CompanyNameLabel.Text = newValue;
        }

        public void ChangedEmployeesCountEvent(int newValue) {
            EmployeeCountLabel.Text = newValue.ToString();
        }

        public void ChangedMonthlyPaymentsEvent(int newValue)
        {
            CostsLabel.Text = newValue.ToString() + " PLN";
        }

        public void ChangedIncomeEvent(int newValue)
        {
            IncomeLabel.Text = newValue.ToString() + " PLN";
        }

        private void GameForm_FormClosed(object sender, FormClosedEventArgs e) {
            Application.Exit();
        }

        private void BuyRoomButton_Click(object sender, EventArgs e) {
            buyRoomForm.Show();
        }

        public void ChangeAllowedRoomsToBuyCount() {
            buyRoomForm.ChangeAllowedRoomsToBuyCount();
        }

        private void AdAgencyButton_Click(object sender, EventArgs e) {
            //adAgencyForm.Width = Screen.FromControl(this).WorkingArea.Width - Width;
            adAgencyForm.Show();
        }

        private void BankButton_Click(object sender, EventArgs e) {
            //bankForm.Width = Screen.FromControl(this).WorkingArea.Width - Width;
            bankForm.Show();
        }

        private void HotelButton_Click(object sender, EventArgs e) {
            //hotelForm.Width = Screen.FromControl(this).WorkingArea.Width - Width;
            hotelForm.Show();
        }

        private void WorkOfficeButton_Click(object sender, EventArgs e)
        {
           // workOfficeForm.Width = Screen.FromControl(this).WorkingArea.Width - Width;
            workOfficeForm.Show();
        }
    }
}