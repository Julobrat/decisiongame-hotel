﻿using System;
using System.Windows.Forms;
using DecisionGame.FrontEnd;

namespace DecisionGame.BackEnd {
    /// <summary>
    /// This class represents basics of decision
    /// </summary>
    public class Decision {
        public bool Completed = false;
        public Control Content;
        public Action defaultDecision = null;

        public int Index;

        public bool IsPermament = true;
        public BaseForm Owner;

        public Game Game => Owner.parent.Game;
    }
}