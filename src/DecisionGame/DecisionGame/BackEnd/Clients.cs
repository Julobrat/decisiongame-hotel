using System;
using System.Linq;

namespace DecisionGame.BackEnd {
    /// <summary>
    /// This simple class simulates clients visiting hotel.
    /// </summary>
    /// <description>
    /// Number of clients if based on popularity and competition.
    /// </description>
    public class Clients {
        private static readonly Random Rnd = new Random();

        public static int[] GetClientsReservations(int popularity, double competition) {
            competition = Math.Min(Math.Max(competition, 0.05), 1.0);
            int minClients = (int)(3 * (1 - competition));
            int maxClients = (int)(10 * (1 - competition)) + 1;
            var clientNumber = Rnd.Next(minClients, maxClients);
            clientNumber = (int) ((clientNumber * ((float) popularity / 100)) * (1-competition));
            var result = from daysToReserve in Enumerable.Range(0, clientNumber) select Rnd.Next(3, 14);
            return result.ToArray();
        }
    }
}