﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DecisionGame.BackEnd;

namespace DecisionGame.FrontEnd {
    public class DecisionsWithControls : DecisionsWithControlsBase
    {
        public Decision CreditDecision;

        public Decision TestDecision;

        public DecisionsWithControls(Game owner) : base(owner)
        {
            creditDecision();
            testDecision();
        }

        private void creditDecision() {
            CreditDecision = new Decision {
                IsPermament = true,
                Owner = Owner.parentForm.bankForm,
                Index = getIndex(),
                Completed = false,
                defaultDecision = null
            };

            var credit = new GroupBox();
            credit.Text = "Kredyt";
            credit.Size = new Size(400, 200);

            var creditButton = new Button();
            creditButton.Text = "Weź kredyt";
            creditButton.Location = new Point((credit.Width - creditButton.Width) / 2, credit.Height - creditButton.Height - 20);

            var creditNumericUpDown = new NumericUpDown();
            creditNumericUpDown.Location = new Point((credit.Width - creditNumericUpDown.Width) / 2, 50);
            creditNumericUpDown.Maximum = int.MaxValue;
            creditNumericUpDown.Increment = 1000;

            var creditTimeNumericUpDown = new NumericUpDown();
            creditTimeNumericUpDown.Location = new Point((credit.Width - creditNumericUpDown.Width) / 2, 110);

            var creditLabel = new Label();
            creditLabel.AutoSize = true;
            creditLabel.Text = "Podaj pożądaną kwotę:";
            creditLabel.Location = new Point(creditNumericUpDown.Location.X, 20);

            var creditTimeLabel = new Label();
            creditTimeLabel.AutoSize = true;
            creditTimeLabel.Text = "Podaj okres kredytowania:";
            creditTimeLabel.Location = new Point(creditNumericUpDown.Location.X, 85);

            var creditInteresetRateLabel = new Label();
            creditInteresetRateLabel.AutoSize = true;
            creditInteresetRateLabel.Text = "Oprocentowanie (miesięcznie): " + Owner.Bank.InterestRate;
            creditInteresetRateLabel.Location = new Point(20, creditTimeNumericUpDown.Location.Y + creditTimeNumericUpDown.Height + 10);

            var creditToPayLabel = new Label();
            creditToPayLabel.AutoSize = true;
            creditToPayLabel.Text = "Do spłaty: " + Owner.Bank.CalculateCredit((int) creditNumericUpDown.Value);
            creditToPayLabel.Location = new Point(20, creditNumericUpDown.Location.Y + creditNumericUpDown.Height + 5);

            void creditButton_onClick(object sender, EventArgs e) {
                Owner.Bank.makeCreditDecision((int) creditNumericUpDown.Value, (int) creditTimeNumericUpDown.Value, CreditDecision.Index);
            }

            creditButton.Click += creditButton_onClick;

            void creditNumericUpDown_ValueChanged(object sender, EventArgs e) {
                creditToPayLabel.Text = "Do spłaty: " + Owner.Bank.CalculateCredit((int) creditNumericUpDown.Value);
            }

            creditNumericUpDown.ValueChanged += creditNumericUpDown_ValueChanged;

            void updateControl(object sender, EventArgs e) {
                creditInteresetRateLabel.Text = "Oprocentowanie (miesięcznie): " + Owner.Bank.InterestRate;
            }

            credit.VisibleChanged += updateControl;

            credit.Controls.Add(creditButton);
            credit.Controls.Add(creditNumericUpDown);
            credit.Controls.Add(creditTimeNumericUpDown);
            credit.Controls.Add(creditLabel);
            credit.Controls.Add(creditTimeLabel);
            credit.Controls.Add(creditInteresetRateLabel);
            credit.Controls.Add(creditToPayLabel);

            CreditDecision.Content = credit;
        }

        private void testDecision() {
            TestDecision = new Decision {
                IsPermament = false,
                Owner = Owner.parentForm.bankForm,
                Index = getIndex(),
                Completed = false
            };

            var credit = new GroupBox();
            credit.Text = "Testowa decyzja";
            credit.Size = new Size(400, 200);

            var testButton = new Button();
            testButton.Text = "podejmij decyzje";
            testButton.Location = new Point((credit.Width - testButton.Width) / 2, credit.Height - testButton.Height - 20);

            var creditNumericUpDown = new NumericUpDown();
            creditNumericUpDown.Location = new Point((credit.Width - creditNumericUpDown.Width) / 2, 50);
            creditNumericUpDown.Maximum = int.MaxValue;
            creditNumericUpDown.Increment = 1000;


            var creditLabel = new Label();
            creditLabel.AutoSize = true;
            creditLabel.Text = "labelka:";
            creditLabel.Location = new Point(creditNumericUpDown.Location.X, 20);

            void testButton_onClick(object sender, EventArgs e) {
                Owner.Bank.testDecision(TestDecision.Index);
            }

            testButton.Click += testButton_onClick;

            credit.Controls.Add(testButton);
            credit.Controls.Add(creditNumericUpDown);
            credit.Controls.Add(creditLabel);

            TestDecision.Content = credit;

            TestDecision.defaultDecision = () => Owner.Bank.testDecision(TestDecision.Index);
        }

        override public Decision[] getDecisions() {
            return new[] {
                CreditDecision,
                TestDecision
            };
        }
    }
}