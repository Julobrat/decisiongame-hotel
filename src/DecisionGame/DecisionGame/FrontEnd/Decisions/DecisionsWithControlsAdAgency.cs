﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DecisionGame.BackEnd;

namespace DecisionGame.FrontEnd {
    public class DecisionsWithControlsAdAgency : DecisionsWithControlsBase {
        public Decision buyAdd;
        public Decision buySpecialAdd;

        public DecisionsWithControlsAdAgency(Game owner) : base(owner) {
            buyAd();
            buySpecialAd();
        }

        private void buyAd()
        {
            buyAdd = new Decision
            {
                IsPermament = true,
                Owner = Owner.parentForm.adAgencyForm,
                Index = getIndex(),
                Completed = false,
                defaultDecision = null
            };

            var hireForm = new GroupBox();
            hireForm.Text = "Kupowanie reklamy";
            hireForm.Size = new Size(400, 300);

            var hireLabel = new Label();
            hireLabel.AutoSize = true;
            hireLabel.Text = "Wpisz kwotę jaką chcesz przeznaczyć na reklamę:";
            hireLabel.Location = new Point((hireLabel.Width) / 2, 20);

            var employeeCountNumericUpDown = new NumericUpDown();
            employeeCountNumericUpDown.Maximum = int.MaxValue;
            employeeCountNumericUpDown.Location = new Point((hireForm.Width - employeeCountNumericUpDown.Width) / 2, 50);
            employeeCountNumericUpDown.Increment = 1000;


            var hireButton = new Button();
            hireButton.Text = "Kup reklamę";
            hireButton.Location = new Point((hireForm.Width - hireButton.Width) / 2, 
                                            employeeCountNumericUpDown.Location.Y + employeeCountNumericUpDown.Height + 20);

            void hireButton_onClick(object sender, EventArgs e)
            {
                int money = (int)employeeCountNumericUpDown.Value;
                if (Owner.AdAgency.CanMakeAd(money))
                {
                    Owner.AdAgency.MakeAd(money);
                    MessageBox.Show("Kupiono reklamę");
                }
                else
                {
                    MessageBox.Show("Nie można kupić reklamy");
                }
            }

            hireButton.Click += hireButton_onClick;

            hireForm.Controls.Add(hireButton);
            hireForm.Controls.Add(employeeCountNumericUpDown);
            hireForm.Controls.Add(hireLabel);

            buyAdd.Content = hireForm;
        }

        private void buySpecialAd()
        {
            buySpecialAdd = new Decision
            {
                IsPermament = false,
                Owner = Owner.parentForm.adAgencyForm,
                Index = getIndex(),
                Completed = false
            };

            int adCost = 20000;

            var credit = new GroupBox();
            credit.Text = "Promocyjna reklama";
            credit.Size = new Size(400, 200);

            var creditLabel = new Label();
            creditLabel.AutoSize = true;
            creditLabel.Text = "Kupić jednorazową reklamę, koszt: " + adCost.ToString() + "?";
            creditLabel.Location = new Point(credit.Location.X, 20);

            var testButton = new Button();
            testButton.Text = "Kup jednorazową reklamę, koszt: " + adCost.ToString();
            testButton.Location = new Point((credit.Width - testButton.Width) / 2, creditLabel.Height + creditLabel.Height + 20);

            void testButton_onClick(object sender, EventArgs e)
            {
                Owner.AdAgency.makeBuyAdDecision(adCost, buySpecialAdd.Index);
            }

            testButton.Click += testButton_onClick;

            credit.Controls.Add(testButton);
            credit.Controls.Add(creditLabel);

            buySpecialAdd.Content = credit;

            buySpecialAdd.defaultDecision = () => Owner.AdAgency.refuseSpecialAd();
        }


        override public Decision[] getDecisions() {
            return new[] {
                buyAdd,
                buySpecialAdd
            };
        }
    }
}