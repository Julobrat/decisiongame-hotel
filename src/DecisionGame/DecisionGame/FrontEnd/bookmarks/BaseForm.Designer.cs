﻿namespace DecisionGame.FrontEnd
{
    partial class BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.DecisionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PerformanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DecisionPanel = new System.Windows.Forms.Panel();
            this.PerformancePanel = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DecisionsToolStripMenuItem,
            this.PerformanceToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(684, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // DecisionsToolStripMenuItem
            // 
            this.DecisionsToolStripMenuItem.Name = "DecisionsToolStripMenuItem";
            this.DecisionsToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.DecisionsToolStripMenuItem.Text = "decyzje";
            this.DecisionsToolStripMenuItem.Click += new System.EventHandler(this.DecisionsToolStripMenuItem_Click);
            // 
            // PerformanceToolStripMenuItem
            // 
            this.PerformanceToolStripMenuItem.Name = "PerformanceToolStripMenuItem";
            this.PerformanceToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.PerformanceToolStripMenuItem.Text = "przeglad";
            this.PerformanceToolStripMenuItem.Click += new System.EventHandler(this.PerformanceToolStripMenuItem_Click);
            // 
            // DecisionPanel
            // 
            this.DecisionPanel.AutoScroll = true;
            this.DecisionPanel.Location = new System.Drawing.Point(43, 54);
            this.DecisionPanel.Name = "DecisionPanel";
            this.DecisionPanel.Size = new System.Drawing.Size(341, 456);
            this.DecisionPanel.TabIndex = 1;
            // 
            // PerformancePanel
            // 
            this.PerformancePanel.Location = new System.Drawing.Point(431, 54);
            this.PerformancePanel.Name = "PerformancePanel";
            this.PerformancePanel.Size = new System.Drawing.Size(344, 456);
            this.PerformancePanel.TabIndex = 2;
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 450);
            this.Controls.Add(this.PerformancePanel);
            this.Controls.Add(this.DecisionPanel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "BaseForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BaseForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BaseForm_FormClosing);
            this.VisibleChanged += new System.EventHandler(this.BaseForm_VisibleChanged);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem DecisionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PerformanceToolStripMenuItem;
        private System.Windows.Forms.Panel DecisionPanel;
        private System.Windows.Forms.Panel PerformancePanel;
    }
}