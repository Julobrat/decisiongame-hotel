using System;
using System.Collections.Generic;
using System.Linq;
using DecisionGame.FrontEnd;

namespace DecisionGame.BackEnd {
    /// <summary>
    /// One of the classes representing bookmarks visible in game. 
    /// </summary>
    /// <description>
    /// It allows user to take loan in the bank window and manages all the computations.
    /// </description>
    public class Bank {
        private readonly Game parent;
        public int AdditionalCreditPerRound;
        public double InterestRate;
        public double InterestRateIncrease;
        public int MaxCredit;
        public Bank(Game parent) {
            this.parent = parent;
        }

        public BankForm BankForm => parent.parentForm.bankForm;

        public bool CanGiveCredit(int money, int creditRounds) {
            return MaxCredit >= money && parent.CurrentRound.RoundNumber + creditRounds <= parent.AllRounds.Length &&
                   creditRounds > 0;
        }

        public int CalculateCredit(int money) {
            return (int) (money * (1 + InterestRate));
        }

        public void GiveCredit(int money, int creditRounds) {
            var instalment = CalculateCredit(money) / creditRounds;
            for (var i = parent.CurrentRound.RoundNumber; i < parent.CurrentRound.RoundNumber + creditRounds; ++i)
                parent.AllRounds[i].Events.Add(new Event(new List<Action> {
                    () => Event.ShowMessage(
                        $"Płacisz {instalment} zł raty kredytu."),
                    () => Event.AddMoney(-instalment, parent.Hotel)
                }));
            MaxCredit -= money;
            parent.Hotel.Money += money;
        }

        public void IncreaseMaxCredit() {
            MaxCredit += AdditionalCreditPerRound;
            InterestRate += InterestRateIncrease;
        }

        public void makeCreditDecision(int money, int creditRounds, int index) {
            if (CanGiveCredit(money, creditRounds)) {
                BankForm.MakeDecision(index);
                var toPay = CalculateCredit(money);
                GiveCredit(money, creditRounds);

                BankForm.Alert("Wzięto kredyt na " + toPay + "zł z okresem spłaty " + creditRounds + " rund");
            }
            else {
                BankForm.Alert("Nie można udzielić kredytu!");
            }
        }

        public void testDecision(int index) {
            BankForm.MakeDecision(index);
            BankForm.Alert("Decyzja podjęta");
        }

        public void PrepareForNextRound() {
            BankForm.PrepareForNextRound();

            IncreaseMaxCredit();
        }

        public bool AreDecisionToMake() {
            return BankForm.decisions.Values.Any(dec => !dec.Completed && !dec.IsPermament);
        }
    }
}