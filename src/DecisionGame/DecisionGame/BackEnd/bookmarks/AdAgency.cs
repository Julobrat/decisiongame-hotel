using System;
using System.Collections.Generic;
using System.Linq;
using DecisionGame.FrontEnd;

namespace DecisionGame.BackEnd {
        /// <summary>
        /// One of the classes representing bookmarks visible in game. 
        /// </summary>
        /// <description>
        /// It makes it possible for the user to spend money on ads increasing popularity of the game.
        /// </description>
    public class AdAgency {
        private readonly Game parent;

        public AdAgency(Game parent) {
            this.parent = parent;
        }
        public AdAgencyForm AdAgencyForm => parent.parentForm.adAgencyForm;
        public double MoneyToPopularityCoefficient = 5000;

        public bool CanMakeAd(int money) {
            return parent.CurrentRound.RoundNumber + 1 <= parent.AllRounds.Length && money <= parent.Hotel.Money;
        }

        public int CalculatePopularity(int money) {
            return (int) (money / MoneyToPopularityCoefficient);
        }

        public void MakeAd(int money) {
            var additionalPopularity = CalculatePopularity(money);
            parent.AllRounds[parent.CurrentRound.RoundNumber].Events.Add(new Event(new List<Action> {
                () => Event.AddPopularity(additionalPopularity, parent.Hotel)
            }));
            parent.Hotel.Money -= money;
        }

        public void makeBuyAdDecision(int money, int index) {
            if (CanMakeAd(money)) {
                AdAgencyForm.MakeDecision(index);
                MakeAd(money);

                var popularityAdded = CalculatePopularity(money);
                var pupularityIncrement = (double)(popularityAdded * 100 / parent.Hotel.Popularity);
                AdAgencyForm.Alert($"Wykupiono reklamę za {money} zł, która zwiększy popularność w przybliżeniu o {pupularityIncrement}%");
            }
            else {
                AdAgencyForm.Alert("Nie można wykupić reklamy!");
            }
        }

        public void PrepareForNextRound() {
            AdAgencyForm.PrepareForNextRound();
        }

        public bool AreDecisionToMake() {
            return AdAgencyForm.decisions.Values.Any(dec => !dec.Completed && !dec.IsPermament);
        }

        internal void refuseSpecialAd()
        {
            parent.Hotel.Popularity -= 5;
            parent.AllRounds[parent.CurrentRound.RoundNumber].Events.Add(new Event(new List<Action> {
                () => Event.ShowMessage("Z powodu nie wykupienia specjalnej reklamy ludzie o tobie powoli zapominają")
            }));
        }
    }
}