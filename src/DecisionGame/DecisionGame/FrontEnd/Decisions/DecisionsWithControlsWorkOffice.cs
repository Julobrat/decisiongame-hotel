﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DecisionGame.BackEnd;

namespace DecisionGame.FrontEnd {
    public class DecisionsWithControlsWorkOffice : DecisionsWithControlsBase {
        public Decision hireEmployeeDec;

        public DecisionsWithControlsWorkOffice(Game owner) : base(owner) {
            hireEmployee();
        }

        private void hireEmployee() {
            hireEmployeeDec = new Decision {
                IsPermament = true,
                Owner = Owner.parentForm.workOfficeForm,
                Index = getIndex(),
                Completed = false,
                defaultDecision = null
            };

            var hireForm = new GroupBox();
            hireForm.Text = "Zatrudnij pracownika";
            hireForm.Size = new Size(400, 300);

            var hireLabel = new Label();
            hireLabel.AutoSize = true;
            hireLabel.Text = "Ile pracowników chcesz zatrudnić:";
            hireLabel.Location = new Point((hireLabel.Width) / 2, 20);

            var employeeCountNumericUpDown = new NumericUpDown();
            employeeCountNumericUpDown.Maximum = 100;
            employeeCountNumericUpDown.Location = new Point((hireForm.Width - employeeCountNumericUpDown.Width) / 2, 50);
            employeeCountNumericUpDown.Increment = 1;

            var salaryLabel = new Label();
            salaryLabel.AutoSize = true;
            salaryLabel.Text = "Ile ma wynosić wynagrodzenie (minimalna = " + Owner.GameSettings.MinimalSalary + "):";
            salaryLabel.Location = new Point((salaryLabel.Width) / 2,
                                              employeeCountNumericUpDown.Location.Y + employeeCountNumericUpDown.Height + 10);

            var salaryCountNumericUpDown = new NumericUpDown();
            salaryCountNumericUpDown.Maximum = int.MaxValue;
            salaryCountNumericUpDown.Value = Owner.GameSettings.MinimalSalary;
            salaryCountNumericUpDown.Location = new Point((hireForm.Width - salaryCountNumericUpDown.Width) / 2, 
                                                           salaryLabel.Location.Y + salaryLabel.Height + 10);
            salaryCountNumericUpDown.Increment = 50;


            var proffesionLabel = new Label();
            proffesionLabel.AutoSize = true;
            proffesionLabel.Text = "Wybierz profesje zatrudnianiych pracowników:";
            proffesionLabel.Location = new Point((proffesionLabel.Width) / 2,
                                                  salaryCountNumericUpDown.Location.Y + salaryCountNumericUpDown.Height + 10);


            var proffesionComboBox = new System.Windows.Forms.ComboBox();
            proffesionComboBox.FormattingEnabled = true;
            proffesionComboBox.Items.AddRange(Owner.workOffice.workers.names);

            proffesionComboBox.Location = new System.Drawing.Point((hireForm.Width - proffesionComboBox.Width) / 2,
                                                                    proffesionLabel.Location.Y + proffesionLabel.Height + 10);


            var hireButton = new Button();
            hireButton.Text = "Zatrudnij";
            hireButton.Location = new Point((hireForm.Width - hireButton.Width) / 2, hireForm.Height - hireButton.Height - 20);

            void hireButton_onClick(object sender, EventArgs e) 
            {
                if(proffesionComboBox.SelectedIndex != -1)
                {
                    Owner.workOffice.hireEmploye((Profession)proffesionComboBox.SelectedIndex, (int)employeeCountNumericUpDown.Value,
                                                 (int)salaryCountNumericUpDown.Value);
                    MessageBox.Show("Pracownicy zostali zatrudnieni: " + employeeCountNumericUpDown.Value.ToString());
                }
                else
                {
                    MessageBox.Show("Musisz wybrać profesje pracowników");
                }
            }

            hireButton.Click += hireButton_onClick;

            hireForm.Controls.Add(hireButton);
            hireForm.Controls.Add(employeeCountNumericUpDown);
            hireForm.Controls.Add(hireLabel);
            hireForm.Controls.Add(salaryLabel);
            hireForm.Controls.Add(salaryCountNumericUpDown);
            hireForm.Controls.Add(proffesionLabel);
            hireForm.Controls.Add(proffesionComboBox);

            hireEmployeeDec.Content = hireForm;
        }

        override public Decision[] getDecisions() {
            return new[] {
                hireEmployeeDec,
            };
        }
    }
}